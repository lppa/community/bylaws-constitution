# Bylaws Constitution
***Libertarian Party of Pennsylvania***

## Description
Includes a simple build script to convert all markdown files to other common document formats.
+ pdf
+ epub
+ odt

## Usage
Edit markdown files and run build.sh

build.sh [-h] <dir>

## Core Dependencies
pandoc
