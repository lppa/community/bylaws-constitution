#!/usr/bin/env bash
##
## A simple script to build documentation from markdown files.
##
## Copyright [2019-2022], [Libertarian Tech]
##
## requires: pandoc


version=0.0.7

defaultDir=${PWD##*/}



arg1=${1:-$defaultDir}
arg2=${2:-}
meName=${0##*/}
usage="$meName $version [help]"



_help () {
  printf '%s\n' "$usage"
  printf '%s\n' "help        -h        Print this message."
  declare -F | awk '{print $3}' | grep -v "^_"
}

build (){ 
  ## Build formats odt, pdf and epub.
  printf "%s\n" "Building $arg1"
  {
  find $arg1 -iname "*.md" -type f -exec sh -c 'pandoc --toc "${0}" -o "${0%.md}.pdf"' {} \; 
  find $arg1 -iname "*.md" -type f -exec sh -c 'pandoc --toc "${0}" -o "${0%.md}.epub"' {} \;
  find $arg1 -iname "*.md" -type f -exec sh -c 'pandoc --toc "${0}" -o "${0%.md}.odt"' {} \;
  } || mkdir -p $arg1
}

[[ $arg1 == -h || $arg1 == *help* ]] && { _help && exit 0 ;}

build
