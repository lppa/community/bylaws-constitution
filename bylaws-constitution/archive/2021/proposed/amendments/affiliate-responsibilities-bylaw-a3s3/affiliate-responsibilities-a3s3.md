# Bylaws

## Article III

### Section 3 – County or Regional Committee Representation on the Board of Directors

Any Affiliate demonstrating that at least twelve (12) LPPA members in good standing are residing in their county or region shall be recognized by the board as having one (1) board vote as a member of the Board of Directors. Only one representative from any committee will be recognized to vote on behalf of the committee. If at any time such county or regional committee shall have fewer than twelve members in good standing of the Party, then the membership committee shall provide the County or Regional Committee Chair a list of members in good standing. The county or regional committee shall have a minimum of 30 days to increase membership or present evidence showing 12 members in good standing before action may be taken or upon a challenge from at least three members in good standing of both the Party and such county or regional committee, such person shall cease to be the County or Regional Committee Representative. Absent such challenge, any duly elected successor board representative of such county or regional committee shall automatically be seated as a member of the Board of Directors.
