# Bylaws

## ARTICLE VIII. COMMITTEES

### Section 1 – Standing Committees

The standing committees of the Party shall be Membership Committee, Media Relations Committee, Election (Ballot Access) Committee, Affiliate Support Committee, Legal Action Committee, Legislative Action Committee, Information Services Committee, and Fundraising (Finance) Committee. The duties, composition, and reporting requirements shall be determined from time to time by the Board of Directors.
