# Bylaws

## Article 1 Purpose and Scope

### Section 1 – Purpose

The purpose of the Party is to conduct the following activities consistent with the
Statement of Principles of the National Libertarian Party:
To elect Libertarians to public office, and move public policy in a Libertarian direction.
