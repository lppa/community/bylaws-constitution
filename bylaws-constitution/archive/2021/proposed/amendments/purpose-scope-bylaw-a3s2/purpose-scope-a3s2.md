# Bylaws

## Article III

### Section 2 – Purpose and Scope of County and Regional Committees
-  To elect Libertarians to public office, and to move public policy in a Libertarian direction.
