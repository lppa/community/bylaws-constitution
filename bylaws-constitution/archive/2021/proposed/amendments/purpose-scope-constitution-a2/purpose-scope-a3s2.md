# Constitution

## Article II Purpose

The purpose of the Party is to conduct the following activities consistent with the
Statement of Principles of the National Libertarian Party:
To elect Libertarians to public office, and move public policy in a Libertarian direction.
