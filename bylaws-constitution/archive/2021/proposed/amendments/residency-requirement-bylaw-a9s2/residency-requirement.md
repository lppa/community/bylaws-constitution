# Bylaws

## Article IX Section 2

Delegates must be a primary resident of Pennsylvania and registered as a Libertarian in Pennsylvania unless prohibited by law.
