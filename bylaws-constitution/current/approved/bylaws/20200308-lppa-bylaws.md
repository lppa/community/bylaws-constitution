﻿% LPPA Bylaws  
% Libertarian Party of Pennsylvania  
% Revised March 8, 2020  


## ARTICLE I. PURPOSE AND SCOPE

### Section 1 – Purpose

The purpose of the Party is to conduct the following activities consistent with the Statement of Principles of the National Libertarian Party:

* Disseminating	Libertarian political philosophy by entering into political information activities.

* Nominating candidates for statewide political office and for offices of the Commonwealth.

* Supporting local and county Libertarian Party candidates.

### Section 2 – Scope

The Party shall conduct its activities primarily within the Commonwealth of Pennsylvania.

## ARTICLE II. MEMBERSHIP

### Section 1 – Establishing a Membership

A person shall become a member of the Party by fulfilling all of the following qualifications:

* Making application.

* Paying such dues as prescribed by the Board of Directors.

* Explicit agreement with the following statement, either by signature or electronic means: "I hereby certify that I do not believe in or advocate the initiation of force or fraud as a means of achieving political or social goals."

### Section 2 – Dues

The Board of Directors may from time to time determine the dues necessary for membership in the Party, and may establish one or more classes of membership with different amounts of dues for each.

### Section 3 – Disciplinary Authority

The Board of Directors shall have the power to discipline a member by a two-thirds vote of the entire Board of Directors.

A member may be disciplined by the Board for cause, such as for failure to maintain all of the qualifications of membership established in Article II Section 1; for misrepresenting the principles of the Party; for endorsing or campaigning, in the name of the Party, for a candidate for public office in opposition to one nominated by the Party; for running for office in the name of the Party or for purporting to have been nominated or endorsed by the Party without having received such nomination or endorsement; or for other reasonable cause.

A member can only be disciplined in one of three ways:

* **Warning**: A member whose actions or inaction results in a warning duly passed by the Board must mitigate damage as directed by the Board, and refrain from repeating the infraction.

* **Censure**: A member whose actions or inaction results in a censure duly passed by the Board is subject to the requirements of a warning, and also becomes ineligible to serve in any elected or appointed capacity within the Party for a period of up to one year (at the discretion of the Board) from the date of the infraction.

* **Suspension**: A member whose actions or inaction results in a suspension duly passed by the Board is subject to the requirements of a warning and a censure, forfeits their membership in the Party, and cannot rejoin the Party for a period of up to one year (at the discretion of the Board) from the date of the infraction.

Notification of any discipline shall be made in writing, and is subject to written appeal as described in Article II Section 4 within fifteen days of notification. Failure to appeal shall result in the immediate imposition of the discipline.

### Section 4 – Discipline Appeal

Upon appeal by the disciplined member, the Judicial Committee shall hold a hearing within 30 days concerning the discipline. Following the hearing, the Judicial Committee shall rule either to affirm or nullify the discipline. Should the Judicial Committee fail to rule, the discipline shall be affirmed.

## ARTICLE III. COUNTY AND REGIONAL COMMITTEES

### Section 1 – Definition of County and Regional Committee

Any group of two or more members in good standing residing in the same county,

shall, upon their request to the Party be recognized by the Board of Directors as a "County Committee". Any group of two or more members in good standing residing in adjoining unrecognized counties, shall, upon their request to the Party, be recognized by the Board of Directors as a "Regional Committee". At least one member in good standing residing in each county included in the regional committee must sign the request for recognition submitted to the Board of Directors. The county or regional committee must select an executive board consisting of two or more people, and they must choose as their Parliamentary Source any version of Robert's Rules. Individual counties that are part of a regional committee may become a county committee through the submittal of a request for recognition as described. Any remaining counties after such recognition shall continue to be recognized as a regional committee regardless of geographic configuration.


### Section 2 – Purpose and Scope of County and Regional Committees

* To elect Libertarians to public office to move public policy in a libertarian direction. County and regional committees shall conduct activities consistent with the Constitution, Bylaws, and Rules of the Party.


### Section 3 – Responsibilities of County and Regional Committees

Any member of a county or regional committee who presents a petition to the Board of Directors from at least twelve members in good standing from the committee shall secure voting privileges for their committee on the Board of Directors. Only one vote will be counted from any recognized committee. 

A county or regional committee that fails to hold a regular meeting in any four month period shall cease to be in good standing. County or regional committees shall be responsible for the nomination and campaign of Libertarian Party candidates for county and local offices. County and regional committees are required to elect their local board and their state Board of Directors representative yearly, and they must provide a rank ordered list of alternate representatives to the board. County or regional committees must file quarterly reports with the Libertarian Party of Pennsylvania secretary in each calendar year documenting any meetings or activity of the committee and must include annually the identity and contact information of the committee officers. Failure to submit two consecutive quarterly reports will result in the suspension of any committee voting representative on the Board of Directors until a report is submitted to the secretary. Failure to submit four consecutive quarterly reports will result in the automatic dissolution of the committee.

### Section 4 – FEC Compliance

Before engaging in federal election activities, County Committees must ensure they are knowledgeable in the applicable rules and regulations. County Committees shall not contribute or expend funds on federal election activities without the prior written approval of the Board of Directors.



### Section 5 – County or Regional Committee Representation on the Board of Directors

If at any time such county or regional committee, with a board vote, shall have fewer than twelve LPPA members in good standing the membership committee shall inform the committee of the lapsed memberships, and provide the County or Regional Committee Chair a list of members in good standing. The county or regional committee shall have a maximum of 60 days to achieve the minimum standard. If a committee has not reached the minimum of 12 members after 60 days, the county or regional committee shall lose voting privileges until such time as they obtain the minimum required. Once a committee has achieved the minimum requirements, board voting privileges shall be restored without need for a board vote.


## ARTICLE IV. OFFICERS

### Section 1 – Chair

The Chair shall preside at all Party conventions and at all meetings of the Board of Directors. He or she shall be the chief executive officer of the Party.

### Section 2 – ViceChairs

Three Vice Chairs, one each from the central, eastern and western portions of Pennsylvania, shall act as assistants to the Chair, with specific emphasis on the founding, development, and growth of county and local organizations.

1. The central portion shall consist of the following 23 counties: Adams, Bedford, Blair, Cameron, Centre, Clearfield, Clinton, Cumberland, Dauphin, Franklin, Fulton, Huntingdon, Juniata, Lycoming, Mifflin, Montour, Northumberland, Perry, Potter, Snyder, Tioga, Union, and York.

2. The eastern portion shall consist of the following 22 counties: Berks, Bradford, Bucks, Carbon, Chester, Columbia, Delaware, Lackawanna, Lancaster, Lebanon, Lehigh, Luzerne, Monroe, Montgomery, Northampton, Philadelphia, Pike, Schuylkill, Sullivan, Susquehanna, Wayne, and Wyoming.

3. The western portion shall consist of the following 22 counties: Allegheny, Armstrong, Beaver, Butler, Cambria, Clarion, Crawford, Elk, Erie, Fayette, Forest, Greene, Indiana, Jefferson, Lawrence, McKean, Mercer, Somerset, Venango, Warren, Washington, and Westmoreland.

### Section 3 – Secretary

The Secretary shall take and keep minutes of all Party conventions and all meetings of the Board of Directors, and shall maintain a current list of all members of the Party.

### Section 4 – Treasurer

The Treasurer shall be responsible for the management of the receipt, disbursement, and accounting of the funds of the Party under the procedures set forth in the Policy Manual. The Treasurer shall not allow the obligations of the

Party to be in excess of the available funds. No member or officer of the Party may create an obligation in the name of the Party without first informing the Treasurer. The Treasurer shall file all required reports on behalf of the Party with government agencies.

### Section 5 – Suspension

An officer may be suspended from office by a two-thirds vote of the Board of Directors for reasons described in Article II Section 3, for failure to fulfill the duties of the office held, or for other reasonable cause. The office of a suspended officer shall be declared vacant unless the suspended officer appeals suspension to the Judicial Committee within ten days of notification of suspension.

### Section 6 – Appeal

Upon written appeal by the suspended officer, the Judicial Committee shall set the date of a hearing. Following the hearing, the Judicial Committee shall rule within three days to either uphold the suspension (thereby vacating the office) or to restore the officer to full authority. Should the Judicial Committee fail to rule, the officer shall be restored to full authority.

### Section 7 – Vacancies

The Board of Directors shall appoint new officers if vacancies or suspensions occur, such officers to complete the term of office vacated. In the instance of Chair vacancy, the longest serving Vice Chair will serve as Interim Chair and be responsible for calling a meeting of the Board of Directors, to be held within 30 days, to select a Chair. In the case of a tie, the Vice Chair with the longest continuing LPPA membership shall fill the role as Interim Chair.

## ARTICLE V. BOARD OF DIRECTORS

### Section 1 – Meeting Notification

The Board of Directors shall meet at such time and place as may be determined by a call of the Chair, or by the request of one third or more of the members of the Board of Directors. Notice of the time and place of the meeting shall be communicated by phone or email to each member of the Board of Directors not less than fourteen days prior to said meeting, unless an earlier date is agreed to by a majority of the Board of Directors. If no meeting of the Board of Directors has been held during a calendar quarter, a meeting shall be held on the last Sunday of said quarter. All meetings of the Board of Directors are open to all members of the party in good standing except when in executive session wherein a motion may not be made or voted upon.

### Section 2 – Transaction of Business

The Board of Directors may, without meeting together, transact business by mail or telephone, by voting on questions submitted to them by or with the approval of the Chair. Fifteen days shall be allowed for the return of votes thereon by mail to the Secretary. If at the expiration of said fifteen days a quorum of the Board of Directors has not returned a vote, the measure being voted upon shall be deemed to have failed; in all other cases a majority of the votes returned shall carry the measure except where a higher vote is required by the Bylaws or Constitution. The Secretary must preserve all such votes until the next meeting of the Board of Directors, at which meeting the Board of Directors shall order disposition of such votes. In the case of a nomination for a special election not conducted at a Board of Directors meeting, the time frame for the return of votes may be limited to as little as three days at the discretion of the chair. All board members must be provided notice of the vote at least three days in advance of the voting deadline. If at the expiration of the voting deadline a majority of board members have not approved the nomination, the nomination shall be deemed to have failed.

### Section 3 – Quorum

An Executive Committee of the Board of Directors shall consist of the officers as defined in Article IV. A quorum of the Board of Directors shall be two-thirds of the Executive Committee or a majority of the Board of Directors.

### Section 4 – Immediate PastChair

The Immediate Past Chair will be the person who most recently served as Chair of the Party. If such person serves in that position for more than one consecutive term, the position of Immediate Past Chair on the Board of Directors will be vacant during the second and succeeding terms.

### Section 5 – Transaction of business over the Internet

Any business of the Board of Directors can be conducted over the Internet in the same manner as via mail or telephone, except that multiple motions may be on the floor simultaneously provided they do not overlap or conflict with one another. If any members of the Board of Directors do not wish to transact Board business over the Internet, when a vote is required the Secretary will forward to them via mail or phone sufficient information regarding the business at hand to allow them to vote knowledgeably.

## ARTICLE VI. EXECUTIVE DIRECTOR

The Board of Directors shall have the power to hire an Executive Director to assist the Chair in administering the operation of the Party. The Executive Director shall be subject to the same qualifications as a member of the Board of Directors. Funds donated to the Party for the specific purposes of ballot access or the Endowment Fund may not be used for compensation of the Executive Director.

## ARTICLE VII. THE JUDICIAL COMMITTEE

### Section 1 – Organization

The Judicial Committee shall elect a chair who shall receive all appeals and petitions and schedule hearings so as to obtain a quorum of the Judicial Committee. When a hearing is requested, the Chair shall be allowed three days to set the date of the hearing.

### Section 2 – Notice

The Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants. If additional parties are added the Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants.

### Section 3 – Limit

Hearings must be held within thirty days from the time the request is received by the Judicial Committee.

### Section 4 – Representation

Each party to a hearing shall have the right to represent his or her interests in the manner of his choosing. The parties to a hearing must include: A) the member(s) requesting the hearing, and B) the member(s), if any, named in the petition or appeal.

### Section 5 – Rulings

The Judicial Committee must provide a ruling within three days of the conclusion of a hearing.

### Section 6 – Vacancies

The Judicial Committee shall appoint new members if vacancies or suspensions occur, such members to complete the term of office vacated.

## ARTICLE VIII. COMMITTEES

### Section 1 – Standing Committees

The standing committees of the Party shall be Membership Committee, Media Relations Committee, Election (Ballot Access) Committee, Legal Action Committee, Legislative Action Committee, Information Services Committee, and Fundraising (Finance) Committee. The duties, composition, and reporting requirements shall be determined from time to time by the Board of Directors.

### Section 2 – Working Committees

There shall be such working committees appointed by the Chair as the Board of Directors deems appropriate. Working committees shall exist at the discretion of the Board of Directors.

## ARTICLE IX. CONVENTION

### Section 1 – Arrangements

It shall be the responsibility of the Board of Directors to set the time, place, and schedule of events of the convention. Publicly accessible notification must be sent to the membership no fewer than 50 days in advance. Email and web site notification shall be used for notification purposes.

### Section 2 – Delegates

Every member in good standing of the Party, as of 180 days before the convention, shall be entitled to be a delegate at such convention by attending in person. However, every attending member in good standing at the time of the convention may be a delegate if two thirds of the delegates present, in person, who meet the aforesaid 180-day requirement, vote to waive said requirement.

Delegates must be registered Libertarian in Pennsylvania unless prohibited by law. 

### Section 3 – CredentialingCommittee

It shall be the responsibility of the party membership committee to credential all pre-registered convention attendees prior to the day of the convention. On the day of the convention, it shall be the responsibility of the membership committee and party treasurer to credential all convention attendees. An accurate count of attendees shall be recorded, as well as an accurate count of all those meeting the

180-day requirement (as aforementioned in Section 2) and those not meeting said requirement. An accurate count of all voting delegates shall then be maintained by this committee throughout the convention until the close of business.

In the absence of a functioning membership committee, it shall be the responsibility of the party chair to appoint a person or persons with access to the party database to act in this capacity.

### Section 4 – National Convention

Delegates to the National Convention shall be chosen at the State Convention immediately prior to the National Convention. Delegates must be LPPA members in good standing.

The delegation Chair will be the LPPA Chair if present and willing; otherwise will be chosen by a majority vote of the state delegation to the National Convention. A secretary will be appointed by the delegation Chair.

Unless the delegates at a Convention of the state Party decide otherwise, all members of the Party shall automatically be alternate delegates at the National Convention, but such members shall serve as delegates only if alternate delegates individually elected at the state Convention are already serving as delegates or are not present on the floor of the National Convention. The members of the delegation present shall have the power to determine which alternates shall fill vacancies in the National Convention delegation, subject to the first sentence.

In the event there are not enough Pennsylvania residents present at the time of the National Convention, out-of-state delegates may be substituted. Out-of-state delegates must be LPPA members in good standing. They will be chosen by a majority vote of the current in-state resident delegates present at the time of voting. The method and time of voting shall be left to the discretion of the delegation Chair, but must be communicated to the standing delegation at least one hour prior to the vote.

## ARTICLE X. NOMINATIONS OF CANDIDATES FOR OFFICE

### Section 1 – Nominations

Only the Libertarian Party of Pennsylvania and its recognized county and regional committees shall have the power to nominate candidates for Pennsylvania elected offices under the "Libertarian Party" label.

1. Candidates for statewide office shall be nominated by delegates in convention for the ensuing election. In the absence of a Convention resolution not to run any candidate for an office, the Board of Directors shall have the power to nominate candidates for elections to be held prior to the next Convention for offices not filled in Convention and for statewide special elections with filing deadlines prior to the next convention.

2. Candidates for district and local office shall be nominated by recognized county or regional committees in good standing. Congressional and state legislative candidates in districts comprising more than one county or regional committee jurisdiction shall be selected by a regional caucus of the recognized county or regional committees in the district.

3. At the request of a Party member in good standing from an area in a multicounty election district having at least one recognized county or regional committee but not included in a recognized county or regional committee, the Executive Committee of the Board of Directors may represent Party members not represented by a county or regional committee in the regional nomination caucus. In such cases, the Executive Committee shall have one vote in the caucus regardless of the number of counties without a recognized committee in the district.

4. In the event of a tie vote in a regional nominating caucus, the tie will be broken by a vote of the LPPA Board of Directors.

5. In the absence of any recognized county or regional committee in an election district or county, the Board of Directors shall have the power to nominate candidates for any public office with the advice of members in that election district or county.

6. The Board of Directors shall have the power to nominate candidates for district and local special elections if county or regional committees fail to act and do not indicate an intention to act on a nomination within two weeks of a filing deadline, nominate substitute candidates as provided in the state election code, and select individuals whose names are to appear on statewide nominating petitions as proxies for President and Vice-President.

7. The Libertarian Party of Pennsylvania or the LPPA Board of Directors shall have the power to withdraw the Party’s endorsement or nomination of candidates for cause.

### Section 2 – Presidential Electors

The Board of Directors shall have the power to select Presidential Electors. Section3– CommitteetoFill Vacancies

The Board of Directors shall have the power to name individuals to serve on the

Committee to Fill Vacancies when required by law.

## ARTICLE XI. STATEMENT OF PRINCIPLES AND PLATFORM

### Section 1 – Statementof Principles

The Statement of Principles affirms that philosophy upon which the Libertarian Party is founded, by which it shall be sustained, and through which liberty shall prevail.

### Section 2 – Platform

The Party Platform shall include the Statement of Principles and the implementation of those principles in the form of planks.

### Section 3 – Amendments

The current Platform shall serve as the basis of all future platforms. At Conventions, the existing Platform may be amended. Additional planks, or changes to planks, must be submitted by the deadline and approved by 2/3 vote. A platform plank may be deleted by majority vote. The enduring importance of the Statement of Principles requires that it may be amended only by a vote of 3/4 of all registered delegates at the Party convention.

### Section 4 – Deadline

All proposed amendments to the Platform must be submitted in writing to the Party Secretary for distribution to the membership at least thirty days prior to the convention.

## ARTICLE XII. AMENDMENTS

These Bylaws may be amended by a majority of all the registered delegates at the Party convention. All proposed amendments to the Bylaws must be submitted in writing to the Party Secretary for distribution to the membership at least thirty days prior to the convention.

## ARTICLE XIII. PARLIAMENTARY AUTHORITY

Robert’s Rules of Order Newly Revised shall be the parliamentary authority for all matters of procedure not specifically covered by these Bylaws.

## ARTICLE XIX. POLICIES AND PROCEDURES

All the policies and procedures adopted by the Board of Directors shall be incorporated into the Policy Manual of the Libertarian Party of Pennsylvania.
