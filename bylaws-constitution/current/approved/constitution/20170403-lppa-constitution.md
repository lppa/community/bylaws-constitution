﻿% LPPA Consitution  
% lIBERTARIAN Party of Pennsylvania  
% Revised April 3rd, 2017  


## ARTICLE I. NAME

The name of this corporation shall be "Libertarian Party of Pennsylvania", hereinafter referred to as the Party.

## ARTICLE II. PURPOSE

The purpose of the Party is to proclaim and to implement the Statement of Principles of the National Libertarian Party by engaging in political and educational activities in the Commonwealth of Pennsylvania.

## ARTICLE III. MEMBERSHIP

An individual endorsing the purposes and principles of the Party may become and remain a member subject to the provisions of the Constitution, Bylaws, and Rules.

## ARTICLE IV. ORGANIZATION Section 1 - Functional Division

The functional division of the Party shall be the County Committee. A member of the Party may join a County Committee subject to the rules and regulations of that Committee.

### Section 2 - Officers

The officers of the Party shall be Chair, Central Vice-Chair, Eastern Vice Chair, Western Vice Chair, Secretary, and Treasurer. All of these officers shall be elected at a convention of the Party by attending delegates and shall take office immediately upon close of such convention.

### Section 3 - Board of Directors

The Board of Directors shall be responsible for the control and management of all the affairs, properties, and funds of the Party consistent with this Constitution, its Bylaws, and any resolutions which may be adopted in convention. No member of the Board of Directors shall at any time cast more than one vote. The members of the Board of Directors of the Party shall be:

- The elected officers of the Party as specified in Article IV Section 2. 
- One representative from each County Committee in good standing.
- Each member (other than alternate members) of the National Committee residing in Pennsylvania and being a member of the Party.
- One member of each of the standing committees defined in the Bylaws (the member to be chosen by that committee).
- The Immediate Past Chair who most recently served as Chair of the Party.

### Section 4 - Judicial Committee

The Judicial Committee shall be composed of five party members elected at a convention of the Party by attending delegates. The term of a member of the Judicial Committee shall run through the period of the next convention. A member of the Judicial Committee may not serve in any Board of Directors position within the Party. The Judicial Committee shall be the final body of appeal in all matters regarding interpretations of the Constitution, Bylaws, and Rules or Resolutions of the Party, subject to the provision that a decision of the Judicial Committee can be overturned by a three quarters vote of the members present at a convention.

### Section 5 - Qualifications, Board of Directors and Judicial Committee

Each member of the Board of Directors or Judicial Committee shall maintain current, paid status as a member of the Libertarian Party of Pennsylvania and maintain status as a voter registered as a Libertarian in the Commonwealth of Pennsylvania, unless prohibited by law. Any Board of Directors or Judicial Committee member whose dues are not current will not be eligible to vote on any matter which shall come before the body of which he is a member. Any Board of Directors or Judicial Committee member who changes his or her voter registration from Libertarian during the term of office shall cease to be a member of that body.

## ARTICLE V. CONVENTION

The Party shall hold an annual convention to conduct such business as may properly come before it at a time and place to be set according to the Bylaws and in compliance with the Constitution, Bylaws, and Rules.

## ARTICLE VI. BYLAWS

The Bylaws are hereby affixed to and subordinate to this Constitution. 

## ARTICLE VII. AMENDMENTS

This Constitution may be amended by a two-thirds vote of all registered delegates at the Party convention. All proposed amendments to the Constitution must be submitted in writing to the Party Secretary at least thirty days prior to the convention.

## ARTICLE VIII. COMPLIANCE

At such time as the Party qualifies to be placed on the Pennsylvania ballot, the Constitution, Bylaws, and Rules of the Party shall be amended to conform with the provisions of the Pennsylvania Election Code. The Board of Directors shall be empowered to take such action as it deems necessary to bring the Constitution, Bylaws, and Rules into compliance with the Election Code.
