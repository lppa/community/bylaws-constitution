﻿% JC Appeals Amendment | LPPA Bylaws  
% LP PENNSYLVANIA Bylaws Committee  
% Revised Jan 31th, 2022  

# Current

## ARTICLE II. MEMBERSHIP

### Section 4 – Discipline Appeal

Upon appeal by the disciplined member, the Judicial Committee shall hold a
hearing within 30 days concerning the discipline. Following the hearing, the
Judicial Committee shall rule either to affirm or nullify the discipline. Should
the Judicial Committee fail to rule, the discipline shall be affirmed.

## ARTICLE IV. OFFICERS

### Section 5 – Suspension

An officer may be suspended from office by a two-thirds vote of the Board of Directors for reasons described in Article II Section 3, for failure to fulfill the duties of the office held, or for other reasonable cause. The office of a suspended officer shall be declared vacant unless the suspended officer appeals suspension to the Judicial Committee within ten days of notification of suspension.

### Section 6 – Appeal

Upon written appeal by the suspended officer, the Judicial Committee shall set the date of a hearing. Following the hearing, the Judicial Committee shall rule within three days to either uphold the suspension (thereby vacating the office) or to restore the officer to full authority. Should the Judicial Committee fail to rule, the officer shall be restored to full authority.

## ARTICLE VII. THE JUDICIAL COMMITTEE

### Section 1 – Organization

The Judicial Committee shall elect a chair who shall receive all appeals and petitions and schedule hearings so as to obtain a quorum of the Judicial Committee. When a hearing is requested, the Chair shall be allowed three days to set the date of the hearing.

### Section 2 – Notice

The Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants. If additional parties are added the Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants.

### Section 3 – Limit

Hearings must be held within thirty days from the time the request is received by the Judicial Committee.

### Section 4 – Representation

Each party to a hearing shall have the right to represent his or her interests in the manner of his choosing. The parties to a hearing must include: A) the member(s) requesting the hearing, and B) the member(s), if any, named in the petition or appeal.

### Section 5 – Rulings

The Judicial Committee must provide a ruling within three days of the conclusion of a hearing.

### Section 6 – Vacancies

The Judicial Committee shall appoint new members if vacancies or suspensions occur, such members to complete the term of office vacated.

# Markup

| Key            |               |
| --------------:| ------------- |
| + Additions    | **bold**                |
| - Subtractions | ~~strike-through text~~ |
| Comments | [this is a comment] |


## ARTICLE II. MEMBERSHIP

### Section 4 – Discipline Appeal

[Moved to Article VII] ~~Upon appeal by the disciplined member, the Judicial Committee shall hold a hearing within 30 days concerning the discipline. Following the hearing, the
Judicial Committee shall rule either to affirm or nullify the discipline. Should
the Judicial Committee fail to rule, the discipline shall be affirmed.~~

  a. **Any disciplined member may file an appeal with Judicial Committee within 10 days of receiving notification of suspension.**

  b. **The Committee shall review a written appeal pursuant to the provisions of the Judicial Rules and limits in Article VII.** 

  c. **Should the Judicial Committee fail to rule, the discipline shall be affirmed.**

## ARTICLE IV. OFFICERS

### Section 5 – Suspension

An officer may be suspended from office by a two-thirds **(2/3)** vote of the Board of Directors for reasons described in Article II Section 3, for failure to fulfill the duties of the office held, or for other reasonable cause. The office of a suspended officer shall be declared vacant unless the suspended officer appeals suspension to the Judicial Committee within ten days of notification of suspension.

### Section 6 – Appeal **of Suspension**

~~Section 6 – Appeal~~

  a. **Any suspended officer may file appeal within ten days of notification of suspension.**

  b. **The Committee shall review a written appeal pursuant to the provisions of the Judicial Rules and limits in Article VII.**

  c. Should the Judicial Committee fail to rule, the officer shall be restored to full authority.

[Moved to Article VII] ~~Upon written appeal by the suspended officer, the Judicial Committee shall set the date of a hearing. Following the hearing, the Judicial Committee shall rule within three days to either uphold the suspension (thereby vacating the office) or to restore the officer to full authority. Should the Judicial Committee fail to rule, the officer shall be restored to full authority.~~


## ARTICLE V. BOARD OF DIRECTORS

### Section 3 – Appeal of Business

   a. **Any member in good standing may appeal a decision of the Board on grounds of LPPA Constitution, Bylaws, Rules non-compliance. The Committee shall review a written appeal pursuant to the provisions of the Judicial Rules and limits in Article VII.**

## ARTICLE VII. THE JUDICIAL COMMITTEE

### Section 1 – Organization

The Judicial Committee shall elect a chair **and a secretary.** 

### Section 2 – **Limits**

  a. **All appeals must include a list the material facts and circumstances surrounding the request.**

  b. **Hearing shall only be set for appeals of discipline, suspension, resolutions, or other actions taken by the Board or its divisions or staff.**

[Moved to Section 3]~~Hearings must be held within thirty days from the time the request is received
by the Judicial Committee.~~

### Section 3 – **Judicial Rules**

  a. **Upon written appeal of a discipline, suspension, or business action the Judicial Committee shall hold a hearing to consider the case in controversy.**

  b. The Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants. If additional parties are added the Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants.

  c. **Considering the quorum requirements, the Chair within 3 days of notice of the appeal shall set a date for the hearing.**

  e. Hearings shall be held within thirty (30) days from the time the appeal is received by the Judicial Committee.

  d. Each party to a hearing shall have the right to represent his or her interests in the manner of his choosing. The parties to a hearing must **at least** include: **1**) the member(s) requesting the hearing, and **2**) the member(s), if any, named in the ~~petition or~~ appeal, and 3) the member(s) implicitly referenced. **Other interested parties may be included at the discretion of the committee.**

  f. **Following a hearing on member discipline, the Judicial Committee shall rule either to affirm or nullify the discipline. Should the Judicial Committee fail to rule, the discipline shall be affirmed.**

  g. The Judicial Committee must provide a ruling within three days of the conclusion of a hearing.

## Section 4 – Vacancies

The Judicial Committee shall appoint new members if vacancies or suspensions occur, such members to complete the term of office vacated.


~~Section 4 – Representation~~ [Moved to Section 3 ]

~~Section 5 – Vacancies~~ [Moved to Section 3 ]

~~Section 6 – Vacancies~~ [Moved to Section 4 ]


# Proposed

## ARTICLE II. MEMBERSHIP

### Section 4 – Discipline Appeal

  a. Any disciplined member may file an appeal with Judicial Committee within 10 days of receiving notification of suspension.

  b. The Committee shall review a written appeal pursuant to the provisions of the Judicial Rules and limits in Article VII.

  c. Should the Judicial Committee fail to rule, the discipline shall be affirmed.

## ARTICLE IV. OFFICERS

### Section 5 – Suspension

An officer may be suspended from office by a two-thirds (2/3) vote of the Board of Directors for reasons described in Article II Section 3, for failure to fulfill the duties of the office held, or for other reasonable cause. The office of a suspended officer shall be declared vacant unless the suspended officer appeals suspension to the Judicial Committee within ten days of notification of suspension.

### Section 6 – Appeal of Suspension

  a. Any suspended officer may file appeal within ten days of notification of suspension.

  b. The Committee shall review a written appeal pursuant to the provisions of the Judicial Rules and limits in Article VII.

  c. Should the Judicial Committee fail to rule, the officer shall be restored to full authority.

## ARTICLE V. BOARD OF DIRECTORS

### Section 3 – Appeal of Business

   a. Any member in good standing may appeal to the Judicial Committee a decision of the Board on grounds of LPPA Constitution, Bylaws, Rules non-compliance. The Committee shall review a written appeal pursuant to the provisions of the Judicial Rules and limits in Article VII.

## ARTICLE VII. THE JUDICIAL COMMITTEE

### Section 1 – Organization

The Judicial Committee shall elect a chair and a secretary.

### Section 2 – Limits

  a. All appeals must include a list the material facts and circumstances surrounding the request.

  b. Hearing shall only be set for appeals of discipline, suspension, resolutions, or other actions taken by the Board or its divisions or staff.

### Section 3 – Judicial Rules

  a. Upon written appeal of a discipline, suspension, or business action the Judicial Committee shall hold a hearing to conciser the case in controversy.

  b. The Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants. If additional parties are added the Judicial Committee must provide at least ten days notice to each of the interested parties to a hearing unless an earlier date is agreed to by the Judicial Committee and the participants.

  c. Considering the quorum requirements, the Chair within 3 days of notice of the appeal shall set a date for the hearing.

  d. Hearings shall be held within thirty (30) days from the time the appeal is received by the Judicial Committee.

  e. Each party to a hearing shall have the right to represent his or her interests in the manner of his choosing. The parties to a hearing must at least include: 1) the member(s) requesting the hearing, 2) the member(s), if any, named in the appeal, and 3) the member(s) implicitly referenced. Other interested parties should be included.

  f. Following a hearing on member discipline, the Judicial Committee shall rule either to affirm or nullify the discipline. Should the Judicial Committee fail to rule, the discipline shall be affirmed.

  g. The Judicial Committee must provide a ruling within three days of the conclusion of a hearing.

## Section 4 – Vacancies

The Judicial Committee shall appoint new members if vacancies or suspensions occur, such members to complete the term of office vacated.


# Reasons

1: Add organization.

2: Remove advisory opinions.
