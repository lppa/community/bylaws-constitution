% Recall Option Amendment | LPPA Bylaws  
% LP Pennsylvania Bylaws Committee  
% Revised Jan 29th, 2022  

# NEW VERSION

_Add before existing Article X_

## ARTICLE X – RECALL VOTES

Any voting member that was present and eligible to vote at the last Convention of the LPPA may submit a written motion to the Executive Committee to recall an Executive Officer or Judicial Committee Member.

1. The motion must be accompanied with a petition of at least 40% of the members that attended that convention and were eligible to vote.

2. A reading of the written motion to request the recall of an Executive Officer or Judicial Committee Member should occur at the next Business Meeting of the LPPA Board of Directors, provided that there are at least fifteen (15) days before the meeting at which such a motion would occur.

3. The Secretary must provide written notification to all members within ten (10) days in advance of the meeting at which the motion should occur.

4. Written notification shall be sent to the last provided email address that is on file for membership.

5. The recall vote passes only if at least two-thirds (2/3) of the eligible voting members of the last LPPA Convention are present (in-person or virtual) and at least two-thirds (2/3) of the eligible voting members of the last LPPA Convention vote in favor of the recall.

6. Upon a recall of an Executive Officer or Judicial Committee Position, the Executive Office or Committee Position is immediately considered vacant.

7. In the event of a requested recall of the Secretary, the Chair must provide written notification to all members within (10) days in advance of the meeting at which the recall vote should occur.
