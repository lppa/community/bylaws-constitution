% Member Privacy Amendment | LPPA Bylaws  
% LP Pennsylvania Bylaws Committee  
% Revised Jan 31th, 2022  

# NEW VERSION

| Key            |                         |
| -------------- | ----------------------- |
| + Additions    | **bold**                |
| - Subtractions | ~~strike-through text~~ |
| [] Comments    | Comments                |

## ARTICLE _ – MEMBER PRIVACY

**Members shall enjoy a reasonable expectation of privacy. The Party will not release Personally Identifiable Information (PII) to the public at large, unless otherwise required by law. Publishing unauthorized PII shall be subject to the disciplinary action.**
