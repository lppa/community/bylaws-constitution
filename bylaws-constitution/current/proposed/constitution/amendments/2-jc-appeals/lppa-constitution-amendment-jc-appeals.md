﻿% JC Appeals Amendment | LPPA CONSTITUTION
% LP Pennsylvania Bylaws Committee
% Revised Jan 31st, 2022


# Current

## ARTICLE IV. ORGANIZATION

### Section 4 - Judicial Committee

The Judicial Committee shall be composed of five party members elected at a convention of the Party by attending delegates. The term of a member of the Judicial Committee shall run through the period of the next convention. A member of the Judicial Committee may not serve in any Board of Directors position within the Party. The Judicial Committee shall be the final body of appeal in all matters regarding interpretations of the Constitution, Bylaws, and Rules or Resolutions of the Party, subject to the provision that a decision of the Judicial Committee can be overturned by a three quarters vote of the members present at a convention.

### Section 5 - Qualifications, Board of Directors and Judicial Committee

Each member of the Board of Directors or Judicial Committee shall maintain current, paid status as a member of the Libertarian Party of Pennsylvania and maintain status as a voter registered as a Libertarian in the Commonwealth of Pennsylvania, unless prohibited by law. Any Board of Directors or Judicial Committee member whose dues are not current will not be eligible to vote on any matter which shall come before the body of which he is a member. Any Board of Directors or Judicial Committee member who changes his or her voter registration from Libertarian during the term of office shall cease to be a member of that body.

# Markup

| Key            | Value                   |
| -------------- | ----------------------- |
| + Additions    | **bold**                |
| - Subtractions | ~~strike-through text~~ |
| [ ] Comments   | [This is a comment.]    |

## ARTICLE IV. ORGANIZATION

### Section 4 - Judicial Committee

The Judicial Committee shall be composed of five party members elected at a convention of the Party by attending delegates. The term of a member of the Judicial Committee shall run through the period of the next convention. A member of the Judicial Committee may not serve in any Board of Directors position within the Party. The Judicial Committee shall be the final body of appeal in all ~~matters~~ **cases in controversy** regarding interpretations of the Constitution, Bylaws, and Rules or Resolutions of the Party, subject to the provision that a decision of the Judicial Committee can be overturned by a three quarters vote of the members present at a convention.

# Proposed

## ARTICLE IV. ORGANIZATION 

### Section 4 - Judicial Committee
The Judicial Committee shall be composed of five party members elected at a convention of the Party by attending delegates. The term of a member of the Judicial Committee shall run through the period of the next convention. A member of the Judicial Committee may not serve in any Board of Directors position within the Party. The Judicial Committee shall be the final body of appeal in all cases in controversy regarding interpretations of the Constitution, Bylaws, and Rules or Resolutions of the Party, subject to the provision that a decision of the Judicial Committee can be overturned by a three quarters vote of the members present at a convention.
